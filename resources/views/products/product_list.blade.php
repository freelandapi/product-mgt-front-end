
@extends('layout.master')
@section('style')
    <style>
        
        .active_page{

            background: #7fad39;
            color: white;  
        }


    </style>
@endsection
@section('content')

    <!-- Categories Section Begin -->
    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{asset('ogani/img/categories/cat-1.jpg')}}">
                            <h5><a href="#">Fresh Fruit</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{asset('ogani/img/categories/cat-2.jpg')}}">
                            <h5><a href="#">Dried Fruit</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{asset('ogani/img/categories/cat-3.jpg')}}">
                            <h5><a href="#">Vegetables</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{asset('ogani/img/categories/cat-4.jpg')}}">
                            <h5><a href="#">drink fruits</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{asset('ogani/img/categories/cat-5.jpg')}}">
                            <h5><a href="#">drink fruits</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container main_product">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".oranges">Oranges</li>
                            <li data-filter=".fresh-meat">Fresh Meat</li>
                            <li data-filter=".vegetables">Vegetables</li>
                            <li data-filter=".fastfood">Fastfood</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter" id="products_content">
                    
                    <!-- products list here -->
            </div>

             <!-- pagination -->
             <nav aria-label="Page navigation example">
                  <ul class="pagination active" id="pagination-list">
                     
                  </ul>
            </nav>



        </div>
    </section>


     <!-- Latest Product Section Begin -->
    <section class="latest-product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Latest Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Top Rated Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Review Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-1.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-2.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{asset('ogani/img/latest-product/lp-3.jpg')}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


      <!-- Blog Section Begin -->
    <section class="from-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title from-blog__title">
                        <h2>From The Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{asset('ogani/img/blog/blog-1.jpg')}}" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                <li><i class="fa fa-comment-o"></i> 5</li>
                            </ul>
                            <h5><a href="#">Cooking tips make cooking simple</a></h5>
                            <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{asset('ogani/img/blog/blog-2.jpg')}}" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                <li><i class="fa fa-comment-o"></i> 5</li>
                            </ul>
                            <h5><a href="#">6 ways to prepare breakfast for 30</a></h5>
                            <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{asset('ogani/img/blog/blog-3.jpg')}}" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                <li><i class="fa fa-comment-o"></i> 5</li>
                            </ul>
                            <h5><a href="#">Visit the clean farm in the US</a></h5>
                            <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script type="text/javascript">

        var totalPage = 0;
        var perPage = 4;
        var currentPage = 1;
    

        $(document).ready(function () {

           
            fetch_data(currentPage);

        });

        function fetch_data(currentPage) {

                let params = {
                    page: currentPage,
                    size: perPage
                }
                
                $.ajax({

                    headers: {
                        'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTg4N2EyOWIzN2ZiZmY2NGY5ZjdkMjk2NmIzZTM4ZWE5OTFhNjA4ZTI3OWI5N2M0ZTJhMGFiMTQ5ZmQ5NmQ3YWRiNzMxNmIxMGY5YzNjZTEiLCJpYXQiOjE1OTk0ODcwNTgsIm5iZiI6MTU5OTQ4NzA1OCwiZXhwIjoxNjMxMDIzMDU4LCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.Sdju0oB6Ne5JTP7BxgH_ymwgwGokP_u9fIYRI7BxTX802CbKdaoVexp1XmF56ePlt1iA9SCaOcIKEqdg04dNlkuqu8ILZblVH5TnJ84BLKPDz1FvxbYiF-4rd8xSuK22nqeFbWPwyOVWW7d0z7g3yBw5fhrJ8fDIzyQqWTAr0M7dTGDsrNm2rkmckP7MckpVwRe1XUI6FKh312OPa0pjtH2eGaqWoq65uPRytvALyz3VJrPOLJayp1GvqNtE1TeL8Mub7j0sWIs3UUbqA7hAPs0olQHhe6wr85ZJwJNj3wHbVJqyqckIMcQaPjvVc9K7PDkMkP0QdWy2M22Vu-62QkVsFH5rOP2vw4uylMxINAAOTTp8vOd6AEHBxOfKFrREzLOsrsTb4H2KlbWu1pV7rcIVjjnwAiYI5rf23lA9SXVuhaSzMQURSo4jeNTKZEhjElTF_teel1zEyesNZNyLZgHAUTdQreUxgWr7tHnCwBaD0_PRgemtgEh13rpaPw8e82t3OcD-JedSMmX9DO8Fx087Oc7HDxjxkmgeqcMzAzSCKJiTbO1YosAYwGrq7Pergq34IV57_RNIMLvtWcePytXMYxSVxRoyHMdEZMyNAtd2MPYOYYbenJIBBaOZBAoqu7Hj0l8x8205cBywit7AgbJ6VztmmVELybBJtCUrnpw',
                        
                    },

                    url: "http://127.0.0.1:8000/api/products",
                    method: "GET",
                    data: params,

                    success:function(res){

                    //console.log("show val:", res);
                    totalPage = res.total_record/perPage;

                   
                    var contents = "";
                        $.each(res.data,function(index,val){

                           
                          

                           
                            contents = '<div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat product_wrapper" data-id="'+val.id+'">'

                                +           '<div class="featured__item" style="border:solid 1px whitesmoke;box-shadow: 2px 2px 2px rgb(192,192,192,0.1)">'
                                        
                                +           '<div class="featured__item__pic set-bg"><img src="'+val.images[0].url+'">'
                                +               '<ul class="featured__item__pic__hover">'
                                +                   '<li><a href="#"><i class="fa fa-heart"></i></a></li>'
                                +                   ' <li><a href="#"><i class="fa fa-retweet"></i></a></li>'
                                +                     '<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>'
                                +               '</ul>'
                                +           '</div>'

                                +           '<div class="featured__item__text">'
                                +               '<h6><a href="#">'+val.title+'</a></h6>'
                                +               '<h5>'+val.original_price+'</h5>'

                                +           '</div>'

                                +         '</div>'
                                +      '</div>';
                          


                               

                              //  $('.featured__filter #products_content').empty().append(contents);
                               
                                $('#products_content').append(contents);
                        });
                       
                       create_pagination(totalPage);
                       onClickPageNumber();
                           
                    },
                    error: function(e){
                        console.log(e)
                    }
            });
        }


        function create_pagination (totalPage){

            let previousPage= '<li class="page-item page-link" id="previousPage">Previous</li>'
            $("#pagination-list").append(previousPage);
            for (let i = 0; i<totalPage; i++) {
                if (i+1 == currentPage) {
                    let page = '<li class="page-item page-link active_page custom-page-item">'+(i+1)+'</li>'
                    $("#pagination-list").append(page);
                }else {
                    let page = '<li class="page-item page-link custom-page-item">'+(i+1)+'</li>'
                    $("#pagination-list").append(page);
                }
                
            }
            let nextPage =  '<li class="page-item page-link" id="nextPage">Next</li>'
            $("#pagination-list").append(nextPage);
            

        }

        function onClickPageNumber() {
            $(".custom-page-item").click(function(){
                $('#products_content').empty();
                $("#pagination-list").empty();
                currentPage = $(this).html();
                fetch_data(currentPage);
            });

            $("#previousPage").click(function(){ 
                if (currentPage > 1) {
                    currentPage--;
                    $('#products_content').empty();
                    $("#pagination-list").empty();
                    fetch_data(currentPage);
                }
            });

            $("#nextPage").click(function(){
                if (currentPage <= totalPage) {
                    currentPage++;
                    $('#products_content').empty();
                    $("#pagination-list").empty();
                    fetch_data(currentPage);
                }
            });
        }

        // get product wrapper id
        $(document).on("click",".product_wrapper",function(){

            var val_id = $(this).attr("data-id");

            window.location.href = "http://127.0.0.1:8001/product_detail?id="+val_id;
        });

        $(document).on("mouseover",".product_wrapper",function(){

            $(this).css('cursor','pointer');
        });

        $(document).on("mouseout",".product_wrapper",function(){

            $(this).css('cursor','auto');
        });

    </script>

            

@endsection