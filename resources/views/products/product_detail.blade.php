
@extends('layout.master_detail')
@section('content')

<style type="text/css">

    .product-count .qtyminus,
    .product-count .qtyplus {
        width: 34px;
        height: 34px;
        background: transparent;
        text-align: center;
        font-size: 19px;
        line-height: 34px;
        color: #000;
        cursor: pointer;
        font-weight: 600;
    }
    .product-count .qtyminus {
        line-height: 32px;
    }
    .product-count .qtyminus {
        border-radius: 3px 0 0 3px;
    }
    .product-count .qtyplus {
        border-radius: 0 3px 3px 0;
    }
    .product-count .qty {
        width: 60px;
        text-align: center;
        border: none;
    }
    .count-inlineflex {
        display: inline-flex;
        border: solid 1px #ccc;
        border-radius: 20px;
        margin-bottom: 10px;
    }

</style>



    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('ogani/img/breadcrumb.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Vegetable’s Package</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <a href="./index.html">Vegetables</a>
                            <span>Vegetable’s Package</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large"
                                src="#" alt="" id="product_img">
                        </div>

                          <div class="product__details__pic__slider owl-carousel" id="product_img_all">
                            <!-- <img data-imgbigurl="{{asset('ogani/img/product/details/product-details-2.jpg')}}"
                                src="{{asset('ogani/img/product/details/thumb-1.jpg')}}" alt="">
                            <img data-imgbigurl="img/product/details/product-details-3.jpg"
                                src="{{asset('ogani/img/product/details/thumb-2.jpg')}}" alt="">
                            <img data-imgbigurl="{{asset('ogani/img/product/details/product-details-5.jpg')}}"
                                src="{{asset('ogani/img/product/details/thumb-3.jpg')}}" alt="">
                            <img data-imgbigurl="{{asset('ogani/img/product/details/product-details-4.jpg')}}"
                                src="{{asset('ogani/img/product/details/thumb-4.jpg')}}" alt=""> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3 id="product_name"></h3>
                        <div class="product__details__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <span>(18 reviews)</span>
                        </div>
                        <div class="product__details__price" id="product_price"></div>
                        <p id="product_desc"></p>

                        <div style="margin-left: 10px;" id="product-option">
{{--                            <div class="row">&nbsp;Color: <span style="color: black; font-weight: bold">Red</span></div>--}}
{{--                            <div class="row" style="margin-top: 5px !important;">--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Red</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Blue</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Dark Blue</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Dark Super Green</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Red</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Blue</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Dark Blue</span>--}}
{{--                                <span class="border border-secondary" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;">Dark Super Green</span>--}}
{{--                            </div>--}}
                        </div>


                       <br>




                        <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <button class="btn btn-default" style="margin-bottom: 6px" id="btn-minus-qty">-</button>
                                    <input type="text" value="1" id="final-qty">
                                    <button class="btn btn-default" style="margin-bottom: 6px" id="btn-plus-qty">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="primary-btn" id="add-to-cart">ADD TO CARD</div>
                        <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a>
                        <ul>
                            <li><b>Availability</b> <span id="product_in_stock"></span></li>
                            <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                            <li><b>Weight</b> <span>0.5 kg</span></li>
                            <li><b>Share on</b>
                                <div class="share">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false">Information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                    aria-selected="false">Reviews <span>(1)</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus
                                        suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam
                                        vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat,
                                        accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a
                                        pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula
                                        elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus
                                        et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam
                                        vel, ullamcorper sit amet ligula. Proin eget tortor risus.</p>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                                        Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed
                                        porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum
                                        sed sit amet dui. Proin eget tortor risus.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                    <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

    <!-- Related Product Section Begin -->
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>Related Product</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{asset('ogani/img/product/product-1.jpg')}}">
                            <ul class="product__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="#">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="img/product/product-2.jpg">
                            <ul class="product__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="#">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="img/product/product-3.jpg">
                            <ul class="product__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="#">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="img/product/product-7.jpg">
                            <ul class="product__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="#">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Related Product Section End -->


    <script type="text/javascript">
        $(document).ready(function(){
            var totalQty = 0; // add-on-qty

            var product_id = getParameterByName("id");
           // console.log("id:",product_id)
            $('#product_option_wrapper').hide();
            $('#product_add_on_wrapper').hide();

           var selectedOption = null
            $.ajax({

                    url: BASE_URL+"products/"+product_id,
                    method: "GET",

                    success:function(res){

                        $("#product_img").attr("src",res.data.images[0].url);
                        $("#product_name").html(res.data.title);
                        $("#product_price").html("$"+res.data.original_price);

                        $("#product_desc").text("Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.");

                        $("#product_in_stock").html("In Stock: "+res.data.in_stock);


                        //Load options
                        if(res.data.options.length>0) {
                            $('#product_option_wrapper').show();

                            $.each( res.data.options, function(i,option){
                                let opt_class = 'proAddOn'+i;
                                let proOptHtml = '<div class="row">&nbsp;'+option.name+' :<span style="color: black; font-weight: bold" class="selected-product-addon'+i+'">'+option.add_ons[0].name+'</span></div>' +
                                    '             <div class="row '+opt_class+'" style="margin-top: 5px !important;">' +
                                    '             </div>';

                                $("#product-option").append(proOptHtml);
                            });

                            //Load addon
                            $.each(res.data.options, function (i, opt){
                                $.each(opt.add_ons, function (index, addon){
                                    let opt_class1 = 'proAddOn'+i;
                                    if (index == 0) {
                                        let add_onHtml ='<span class="border border-secondary product-addon" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; background-color: #7fad39; color: white; cursor: pointer;" option-index="'+i+'">'+addon.name+'</span>';
                                        $('.'+opt_class1+'').append(add_onHtml);
                                    } else {
                                        let add_onHtml ='<span class="border border-secondary product-addon" style="margin: 5px; padding: 5px;border-radius: 5px; font-size: 14px; cursor: pointer;" option-index="'+i+'">'+addon.name+'</span>';
                                        $('.'+opt_class1+'').append(add_onHtml);
                                    }

                                });
                            });

                        }


                        $.each( res.data.images, function(key,img){

                            console.log("img:",img.url);
                            var html = '<img src="'+img.url+'" style="width:120px;height:120px;margin-left:5px" class="img-thumbnail">';
                            $("#product_img_all").append(html);
                        });

                    },

                    error: function(e){
                        console.log("data error:",e)
                    }
            });


           //ON Click selected add on
           $(document).on("click",".product-addon",function (){
               var addOnVal = $(this).html();

               let optionIndex = $(this).attr('option-index');
               let selectedAddOnClass = 'selected-product-addon'+optionIndex+'';

               $('.proAddOn'+optionIndex).find('.product-addon').css({'background-color': 'white', 'color': 'black'});
               $(this).css({ "background-color": "  #7fad39", "color": "white" });


               $('.'+selectedAddOnClass+'').html(addOnVal)

           });


            //ON click btn minus
            $(document).on("click", "#btn-minus-qty", function(){
                var textField = $(this).parent().find('#final-qty')
                var value = parseInt(textField.val());
                if (value < 2) {
                    value = 1;
                } else {
                    value --;

                    totalQty -= 1
                  $('#final-qty').val(totalQty);
                   // finalQty.val(totalQty);

                }

                textField.val(value);
            })


            //ON click plus btn
            $(document).on("click", "#btn-plus-qty", function(){
                var textField = $(this).parent().find('#final-qty')
                var value = parseInt(textField.val());
                value ++;
                textField.val(value);

                totalQty += 1
                $('#final-qty').val(totalQty);


            })



            //ON click ADD to cart
            $(document).on("click","#add-to-cart",function(){

                var obj = {
                    id: parseInt(product_id),
                    title: $("#product_name").html(),
                    original_price: $("#product_price").html().substring(1)
                }

                console.log("current object = ", obj);

                //localStorage.setItem('testObject', JSON.stringify(testObject));
            });

            $(document).on("mouseover","#add-to-cart",function(){

                $(this).css('cursor','pointer');
            });


        });

    </script>









@endsection
