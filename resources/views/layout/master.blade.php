<!DOCTYPE html>
<html lang="zxx">
 

<head>
    
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    

    <title>Ogani | Template</title>


    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('ogani/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('ogani/css/style.css')}}" type="text/css">


   

    @yield('style')
    
</head>

<body>

    @include('layout.header')

    @include('layout.side_bar')



    @yield('content')

    @include('layout.footer')



 <!-- Js Plugins -->
    <script src="{{asset('ogani/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('ogani/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('ogani/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('ogani/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('ogani/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('ogani/js/mixitup.min.js')}}"></script>
    <script src="{{asset('ogani/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('ogani/js/main.js')}}"></script>


    
  <!-- Page Preloder -->
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->

   



</body>

</html>